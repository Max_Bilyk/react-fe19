import styled from "styled-components";

export const Icon = styled.i`
  width: 20px;
  height: 20px;
`

export const FavIcon = styled.i`
  width: 20px;
  height: 20px;
`