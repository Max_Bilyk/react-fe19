import React from "react";
import axios from "axios";
import ProdCard from "../ProductCard";
import {CardWrap} from '../../styles/listStyles';

class ProdList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cardData: [],
            favourites: [],
            inCart: []
        }

        this.setToFav = this.setToFav.bind(this);
        this.setToCart = this.setToCart.bind(this);
        this.removeFromFav = this.removeFromFav.bind(this);
    }

    async componentDidMount() {
        const {data} = await axios.get('http://localhost:3000/products.json');
        this.setState({cardData: data});
    }

    async setToFav(item) {
        await this.setState(prevState => ({favourites: [...prevState.favourites, item]}))
    }

    async setToCart(item) {
        await this.setState(prevState => ({inCart: [...prevState.inCart, item]}))
    }

    removeFromFav(item) {
        let idx = item.id;
        let favList = this.state.favourites;
        this.setState(() => {
            const favourites = favList.filter(item => item.id !== idx);
            console.log('Remove --->', favourites);
            return {favourites};
        });
        localStorage.removeItem("Favourites")
    }

    render() {
        return (
            <CardWrap className="card-wrapper">
                {this.state.cardData.map(item =>
                    <ProdCard
                        src={item.src}
                        productName={item.productName}
                        color={item.color}
                        price={item.price}
                        vendorCode={item.vendorCode}
                        key={item.id}
                        card={item}
                        setToFav={this.setToFav}
                        removeFromFav={this.removeFromFav}
                        favArr={this.state.favourites}
                        cartArr={this.state.inCart}
                        setToCart={this.setToCart}
                    />)
                }
            </CardWrap>
        )
    }
}

export default ProdList;