import React from "react";
import ProdList from "./components/ProductList/";
import './App.css';
localStorage.clear();

class App extends React.Component {
    render() {
        return (
            <div className="container">
                <ProdList/>
            </div>
        )
    }
}

export default App;
