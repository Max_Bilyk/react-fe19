import React from 'react';
import Button from "./components/Button";
import Modal from "./components/Modal";
import './App.css'

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modalActive: false,
            setActiveModal: false,
            modalId: null,
        }

        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    showModal(e) {
        console.log(e.target.dataset);
        this.setState({
            modalActive: true,
            setActiveModal: true,
            modalId: e.target.dataset.modal,
        })
    }

    hideModal() {
        this.setState({
            modalActive: false,
            setActiveModal: false
        })
    }

    render() {
        return (

            <div className="wrapper">
                <Button
                    dataModal="#modal1"
                    btnClass="btn btn-primary modal1"
                    bgColor="#0d6efd"
                    btnText="Open first modal"
                    onClick={this.showModal}
                    textColor="#ffffff"
                />
                <Button
                    dataModal="#modal2"
                    btnClass="btn btn-warning"
                    bgColor="#ffc107"
                    btnText="Open second modal"
                    onClick={this.showModal}
                    textColor="#000000"
                />

                {this.state.modalId === '#modal1' ?
                    <Modal
                        active={this.state.modalActive}
                        onModalClose={this.hideModal}
                        header='Do you want to delete this file?'
                        text='Once you delete this file, it won’t be possible to undo this action.'
                        subText='Are you sure you want to delete it?'
                        actions={[
                            <Button
                                btnClass="btn modal-btn"
                                bgColor="#B3382C"
                                btnText="Ok"
                                onClick={this.hideModal}
                                textColor="#FFFFFF"
                            />,
                            <Button
                                btnClass="btn modal-btn"
                                bgColor="#B3382C"
                                btnText="Cancel"
                                onClick={this.hideModal}
                                textColor="#FFFFFF"
                            />
                        ]}
                        modalTheme={{
                            headerBg: '#D44637',
                            mainBg: '#E74C3C',
                            footerBg: '#E74C3C',
                            headerColor: '#ffffff',
                            mainColor: '#ffffff',
                            closeBtnColor: '#ffffff'
                        }}
                        id="modal1"
                    /> :
                    <Modal
                        active={this.state.modalActive}
                        onModalClose={this.hideModal}
                        header='Header'
                        text='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, nulla!'
                        subText='Lorem ipsum dolor sit amet, consectetur?'
                        actions={[
                            <Button
                                btnClass="btn modal-btn"
                                bgColor="#1a1e21"
                                btnText="Submit"
                                onClick={this.hideModal}
                                textColor="#FFFFFF"
                            />,
                            <Button
                                btnClass="btn modal-btn"
                                bgColor="#565e64"
                                btnText="Cancel"
                                onClick={this.hideModal}
                                textColor="#FFFFFF"
                            />
                        ]}
                        id='modal2'
                        modalTheme={{
                            headerBg: '#4c0a70',
                            mainBg: '#7d32a6',
                            footerBg: '#7d32a6',
                            headerColor: '#fff',
                            mainColor: '#fff',
                            closeBtnColor: '#fff'
                        }}
                    />
                }
            </div>
        )
    };
}


export default App;