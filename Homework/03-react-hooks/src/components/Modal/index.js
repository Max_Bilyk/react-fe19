import {
    ModalHeader,
    ModalBody,
    ActionsButtonWrap,
    ModalWindow,
    ModalContent,
    CloseBtn,
    Close
} from '../../styles/modalStyles';
import './Modal.css'


function Modal(props) {
    const {active, onModalClose, header, text, subText, actions, modalTheme, id} = props;
    const style = {
        borderTop: 'none',
        borderBottom: 'none',
    }

    return (
        <ModalWindow className={active ? 'modal active' : 'modal '} onClick={onModalClose} id={id}>
            <ModalContent className={active ? 'modal__content active' : 'modal__content'} onClick={(e) => {
                e.stopPropagation()
            }}>
                <ModalHeader modalTheme={modalTheme} style={style} className="modal-header">
                    <h5 className="modal-title">{header}</h5>
                    <CloseBtn type="button" className="btn">
                        <Close modalTheme={modalTheme} onClick={onModalClose}>&times;</Close>
                    </CloseBtn>
                </ModalHeader>
                <ModalBody modalTheme={modalTheme} className="modal-body">
                    <p>{text}</p>
                    <p>{subText}</p>
                </ModalBody>
                <ActionsButtonWrap modalTheme={modalTheme} style={style} className="modal-footer">
                    {actions[0]}
                    {actions[1]}
                </ActionsButtonWrap>
            </ModalContent>
        </ModalWindow>
    )
}


export default Modal;