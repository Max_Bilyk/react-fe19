import {CardWrap} from '../../styles/cardListStyles';
import {useState, useEffect} from "react";
import ProdCard from "../ProdCard";
import axios from "axios";


function ProdList() {
    const [cardData, setCardData] = useState([]);
    const [favourites, setFavourites] = useState(null);
    const [cart, setCart] = useState(null);

    useEffect(() => {
           async function fetchData() {
               const {data} = await axios.get('http://localhost:3000/products.json');
               setCardData(data);
           }
           fetchData();

        const localFav = JSON.parse(localStorage.getItem("Favourites"));
        const localCart = JSON.parse(localStorage.getItem("Cart"));

        localFav === null
            ? setFavourites([])
            : setFavourites(localFav);

        localCart === null
            ? setCart([])
            : setCart(localCart);

    }, []);

    useEffect(() => {
        localStorage.setItem("Favourites", JSON.stringify(favourites));
    }, [favourites]);

    useEffect(() => {
        localStorage.setItem("Cart", JSON.stringify(cart));
    }, [cart]);

    const setToFav = item => {
        setFavourites([...favourites, item]);
    };

    const setToCart = item => {
        setCart([...cart, item]);
    };

    const removeFromStorage = (item, removeItem) => {
        const itemId = item.id;

        if (removeItem === favourites){
            setFavourites(favourites.filter(({id}) => itemId !== id));
        }else if (removeItem === cart){
            setCart(cart.filter(({id}) => itemId !== id));
        }
    }

    return (
        <CardWrap className="card-wrapper">
            {cardData.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    favourites={favourites}
                    cart={cart}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    removeFromStorage={removeFromStorage}
                    isFavourite={false}
                />)
            }
        </CardWrap>
    )
}

export default ProdList;