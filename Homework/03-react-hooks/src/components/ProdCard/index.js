import {CardImage, Card, CardBody, CardButtonsWrap,} from '../../styles/cardStyles'
import Button from '../Button';
import ModalContainerAdd from "../../containers/modalContainerAdd";
import ModalContainerRemove from "../../containers/modalContainerRomove";
import {useState} from "react";


function ProdCard(props) {
    const {
        src,
        productName,
        color,
        price,
        vendorCode,
        card,
        setToFav,
        setToCart,
        removeFromStorage,
        cart,
        favourites,
        isFavourite
    } = props;
    const [isProductFav, setFavProduct] = useState(isFavourite);
    const [isInCart, setInCart] = useState(false);
    const [isModalActive, setModalActive] = useState(false);
    const [modalId, setModalId] = useState(null);

    // useEffect(() => {
    // isProductFav ? console.log('true') : console.log('false');
    // }, [isProductFav, favourites])

    const addToFav = () => {
        setFavProduct(!isProductFav);
    }

    const addToCart = () => {
        setInCart(!isInCart);
    }

    const showModal = e => {
        setModalActive(true);
        setModalId(e.target.dataset.modal);
        console.log(modalId);
    }

    const hideModal = () => {
        setModalActive(false);
    }


    return (
        <Card className="card" style={{width: '18rem'}}>
            <CardImage className="card-img-top" src={src}/>
            <CardBody className="card-body">
                <h5 className="card-title">{productName}</h5>
                <p className="card-text">Color: {color}</p>
                <p className="card-text">Price: {price}</p>
                <p className="card-text">Vendor Code: {vendorCode}</p>

                <CardButtonsWrap>
                    {!isInCart &&
                    <>
                        <Button
                            dataModal={`modal${card.id}`}
                            btnClass="btn card-btn"
                            onClick={showModal}
                            bgColor="#0a58ca"
                            btnText="Add to cart"
                            textColor="#ffffff"
                        />
                        <ModalContainerAdd
                            id={`modal${card.id}`}
                            activeState={isModalActive}
                            onModalHide={hideModal}
                            onAddToCartClick={() => {
                                setToCart(card)
                                addToCart();
                                hideModal();
                            }}
                        />
                    </>
                    }
                    {isInCart &&
                    <>
                        <Button
                            dataModal={`modal${card.id}`}
                            btnClass="btn card-btn"
                            onClick={showModal}
                            bgColor="#dc3545"
                            btnText="Remove from cart"
                            textColor="#ffffff"
                        />
                        <ModalContainerRemove
                            id={`modal${card.id}`}
                            activeState={isModalActive}
                            onModalHide={hideModal}
                            onRemoveFromCartClick={() => {
                                removeFromStorage(card, cart)
                                addToCart();
                                hideModal();
                            }}
                        />
                    </>
                    }
                    <button className="btn btn-warning">
                        {!isProductFav &&
                        <i
                            className="far fa-star"
                            onClick={() => {
                                setToFav(card);
                                addToFav();
                            }}
                        />}
                        {isProductFav && <i
                            onClick={() => {
                                removeFromStorage(card, favourites);
                                addToFav();
                            }}
                            className="fas fa-star"
                        />}
                    </button>
                </CardButtonsWrap>
            </CardBody>
        </Card>
    )
}

export default ProdCard;