import {CardWrap} from "../../styles/cardListStyles"
import ProdCard from "../../components/ProdCard";

const Favourites = () => {
    const favourites = JSON.parse(localStorage.getItem("Favourites"));

    return (
        <>
            <h3>{favourites ? favourites.length : 0} Favourites items</h3>
            <CardWrap>
                {favourites
                    ? favourites.map(item =>
                        <ProdCard
                            src={item.src}
                            productName={item.productName}
                            color={item.color}
                            price={item.price}
                            vendorCode={item.vendorCode}
                            key={item.id}
                            card={item}
                            isFavourite={true}

                        />)
                    : null
                }
            </CardWrap>
        </>
    )
}

export default Favourites;