import {CardWrap} from "../../styles/cardListStyles";
import ProdCard from "../../components/ProdCard";

const Cart = () => {
    const cart = JSON.parse(localStorage.getItem("Cart"));

    return (
        <>
            <h3>{cart ? cart.length : 0} Items in cart</h3>

            <CardWrap>
                {cart
                    ? cart.map(item =>
                        <ProdCard
                            src={item.src}
                            productName={item.productName}
                            color={item.color}
                            price={item.price}
                            vendorCode={item.vendorCode}
                            key={item.id}
                            card={item}
                        />
                    )
                    : null
                }
            </CardWrap>
        </>
    )
}

export default Cart;