import Modal from "../../components/Modal";
import Button from "../../components/Button";
import PropTypes from "prop-types";
import ModalContainerAdd from "../modalContainerAdd";

function ModalContainerRemove(props) {
    const {activeState, onModalHide, card, onRemoveFromCartClick} = props;

    return (
        <Modal
            active={activeState}
            onModalClose={onModalHide}
            header='Do you want to remove this product from cart?'
            text='Once you remove this product, it won’t be possible to undo this action. But you can add this product to cart again'
            subText='Are you sure you want to remove it?'
            actions={[
                <Button
                    btnClass="btn modal-btn"
                    bgColor="#B3382C"
                    btnText="Ok"
                    onClick={onRemoveFromCartClick}
                    textColor="#FFFFFF"
                />,
                <Button
                    btnClass="btn modal-btn"
                    bgColor="#B3382C"
                    btnText="Cancel"
                    onClick={onModalHide}
                    textColor="#FFFFFF"
                />
            ]}
            modalTheme={{
                headerBg: '#D44637',
                mainBg: '#E74C3C',
                footerBg: '#E74C3C',
                headerColor: '#ffffff',
                mainColor: '#ffffff',
                closeBtnColor: '#ffffff'
            }}
            id={card}
        />
    )
}

ModalContainerRemove.propTypes = {
    activeState: PropTypes.bool,
    onModalHide: PropTypes.func,
    card: PropTypes.object,
    onAddToCartClick: PropTypes.func,
}

export default ModalContainerRemove;