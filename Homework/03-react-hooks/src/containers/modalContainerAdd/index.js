import Modal from "../../components/Modal";
import Button from "../../components/Button";
import PropTypes from "prop-types";

function ModalContainerAdd(props) {
    const {activeState, onModalHide, card, onAddToCartClick} = props;

    return (
        <Modal
            active={activeState}
            onModalClose={onModalHide}
            header='Do you want to add this product to cart?'
            text='If you would add this product to cart, it will be written in local storage'
            subText='Are you sure you want to add it to cart?'
            actions={[
                <Button
                    btnClass="btn modal-btn"
                    bgColor="#1a1e21"
                    btnText="Submit"
                    onClick={onAddToCartClick}
                    textColor="#FFFFFF"
                />,
                <Button
                    btnClass="btn modal-btn"
                    bgColor="#565e64"
                    btnText="Cancel"
                    onClick={onModalHide}
                    textColor="#FFFFFF"
                />
            ]}
            modalTheme={{
                headerBg: '#4287f5',
                mainBg: '#0572f0',
                footerBg: '#0572f0',
                headerColor: '#000000',
                mainColor: '#fff',
                closeBtnColor: '#fff'
            }}
            id={card}
        />
    )
}

ModalContainerAdd.propTypes = {
    activeState: PropTypes.bool,
    onModalHide: PropTypes.func,
    card: PropTypes.object,
    onAddToCartClick: PropTypes.func,
}

export default ModalContainerAdd;