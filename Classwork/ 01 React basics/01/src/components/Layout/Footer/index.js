import React from 'react';
import Menu from "../../common/Menu";
import './Footer.css'

function Footer() {
    return (
        <div className='footer'>
            <section>Logo section</section>
            <section>
                <h2>List:</h2>
                <Menu/>
            </section>
            <section>Other type of section</section>
        </div>
    )
}

export default Footer;