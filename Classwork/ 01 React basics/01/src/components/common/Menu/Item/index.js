import React from 'react';

function MenuItem({url, title, style}) {
    return (
        <li className={style === 'light' ? 'light-item' : 'not-light'}>
            <a href={url}>{title}</a>
        </li>
    )
}

export default MenuItem;