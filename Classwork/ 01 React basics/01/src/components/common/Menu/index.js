import React from 'react'
import MenuItem from "./Item";
import './Menu.css';

function Menu(props) {

    const {style = 'light'} = props;

    const items = [
        {url: '1', title: 'Menu item 1', style},
        {url: '2', title: 'Menu item 2', style},
        {url: '3', title: 'Menu item 3', style},
        {url: '4', title: 'Menu item 4', style},
        {url: '5', title: 'Menu item 5', style},
    ];

    // setTimeout(() => {
    //     items.push({url: 'new', title:'Additional'})
    // }, 2500)

    return (
        // <React.Fragment>
        <div className='menu-wrapper'>
            <ul>
                {
                    items.map(item => <MenuItem key={item.url}
                                                url={item.url}
                                                title={item.title}
                                                style={item.style}/>)
                }

            </ul>
        </div>
        // </React.Fragment>
    )
}

export default Menu;