import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Modal.css'
import Button from "../Button";

class Modal extends Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this)
    }

    onClose() {
        this.props.onClose();
    }

    render() {
        const style = {
            display: this.props.show ? 'block' : 'none'
        }
        return (
            <>
                <div style={style} className="modal-overlay">

                </div>
                <div style={style} className='modal'>
                    <div className='modal-header'>Header
                        {this.props.title}
                    </div>
                    <div className='modal-body'>Body
                        {this.props.children}
                    </div>
                    <div className='modal-footer'>Footer
                        < Button title='close' onClick={this.onClose}/>
                    </div>
                </div>
            </>
        );
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    show: PropTypes.bool,
    onshow: PropTypes.func
}
export default Modal;