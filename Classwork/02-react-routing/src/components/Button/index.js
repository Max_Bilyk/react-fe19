import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Button extends Component {
    constructor(props) {
        super(props)
        this.onBtnClick = this.onBtnClick.bind(this)
    }

    onBtnClick() {
        this.props.onClick();
    }

    render() {
        return (
            <button onClick={this.onBtnClick}>{this.props.title}Button</button>
        )
    }
}

Button.propTypes = {
    onBtnClick: PropTypes.func,
    title: PropTypes.string
}
export default Button