import React from "react";

function PeopleList({items, colored}) {
    const style = {
        backgroundColor: colored ? 'yellow' : 'red',
    }

    return (
        <ul style={style} className="list">
            {items.map(item =>
                <li key={item.name}>Name: {item.name}</li>
            )}
        </ul>
    )
}

export default PeopleList;