import React from 'react';
import UserCard from "../UserCard";
import UserService from "../../Services/userService";

function UserList () {
    const users = UserService.loadUser();

    return (
        <div className="user-list card-group">
            {users.map(user => (<UserCard user={user} key={user.id}/>))}
        </div>
    );
}

export default UserList;