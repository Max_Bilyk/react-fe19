import React from 'react';

function UserCard(props) {
    const {user} = props;

    return (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{user.fullUserName}</h5>
                    <p className="card-text">{`Some description: ${user.descr}`}</p>
                    <p className="card-text">{user.userBirthday}</p>
                    <p className="card-text"><small className="text-muted">{`City: ${user.city}`}</small></p>
                    <p className="card-text"><small className="text-muted">{`Specialization: ${user.department}`}</small></p>
                </div>
        </div>
    )
}

export default UserCard;