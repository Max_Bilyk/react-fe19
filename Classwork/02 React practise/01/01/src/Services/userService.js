import users from '../Data/users.json';
import {userModel} from "./models";

class UserService {
    constructor() {}

    static loadUser () {
        return users.map(user => new userModel(user))     ;
    }
}

export default UserService;