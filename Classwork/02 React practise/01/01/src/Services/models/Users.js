import moment from "moment";

class User {
    constructor(
        {
            first_name,
            last_name,
            middle_name,
            birthday,
            description,
            city,
            department,
            id
        }
    )
    {
        this.firstName = first_name;
        this.lastName = last_name;
        this.middleName = middle_name;
        this._bd = birthday;
        this.descr = description;
        this.city = city;
        this.department = department;
        this.id = id;
    }

    get fullUserName () {
        return `${this.firstName} ${this.lastName} ${this.middleName}`
    }

    get userBirthday () {
        return this._bd ? 'User birthday: ' + moment(this._bd).format('DD.MM.YYYY') : 'n/a'
    }
}

export default User;