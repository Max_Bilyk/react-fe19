import React, {Component} from 'react';
import './Counter.css';

class Counter extends Component {
    constructor(props) {
        super(props);
        const {init = 0, text = ''} = this.props;

        this.state = {
            loading: true,
            counter: init,
            text: text,
        }

        this.onCountClick = this.onCountClick.bind(this);
    }

    onCountClick() {
        this.setState({
            counter: this.state.counter + 1,
        });

    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                loading: false,
            })
        },2500)
    }

    render() {
        return (
            <>
                <div className='counter-wrapper'>
                    {this.state.loading
                        ? (
                            <div>Loading...</div>
                        )
                        : (
                            <>
                                <div className="counter-statistic">{this.state.counter}</div>
                                <button onClick={this.onCountClick}>{this.state.text} click me</button>
                            </>
                        )
                    }
                </div>
            </>
        );
    }
}

export default Counter;