import {useState, useRef} from "react"

export default function FormHandMade({onSubmit}) {
    const [data, setData] = useState({});

    /** Form using libraries like Formik and validations form lib - Yup **/

    const onFormSubmit = e => {
        e.preventDefault();
        if (typeof onSubmit === 'function') {
            onSubmit(data)
        }
    }

    const onElementChange = ({target: {name, value}}) => {
        setData(
            {
                ...data,
                ...{
                    [name]: value,
                }
            })
    }

    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <div className="form-item">
                    <label htmlFor="email">Email:</label>
                    <input
                        id="email"
                        name="email"
                        minLength="3"
                        type="text"
                        onChange={onElementChange}
                        value={data.email}
                    />
                </div>
                <div className="form-item">
                    <label htmlFor="name">Name:</label>
                    <input
                        id="name"
                        minLength="3"
                        name="name"
                        type="text"
                        onChange={onElementChange}
                        value={data.name}
                    />
                </div>
                <div className="form-item">
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}