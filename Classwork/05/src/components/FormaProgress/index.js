import {Formik, Form, Field} from "formik";
import * as Yup from "yup";

const subscriptionSchema = Yup.object().shape({
    name: Yup.string()
        .required("Required")
        .min(6, 'Name must contain at least 6 characters'),
    email: Yup.string()
        .email("Enter a valid email")
        .required("Email is required"),
});
export default function FormProgress({onSubmit}) {
    const onSubmitForm = (data) => {
        onSubmit(data);
    }
    return (
        <div className='progressive-form'>
            <Formik
                initialValues={{
                    name: '',
                    email: '',
                }}
                validationSchema={subscriptionSchema}
                onSubmit={onSubmitForm}
            >
                {
                    ({
                         errors, touched
                     }) => (

                        <Form>
                            <div>
                                <label>Name</label>
                                <Field name="name"/>
                                {errors.name && touched.name ? (
                                    <div style={{
                                        color: 'red'
                                    }}>{errors.name}</div>
                                ) : null}
                            </div>
                            <div>
                                <label>Email</label>
                                <Field name="email" type="email"/>
                                {errors.email && touched.email ? <div style={{
                                    color: 'red'
                                }}>{errors.email}</div> : null}
                            </div>
                            <button type="submit">Submit</button>
                        </Form>
                    )
                }
            </Formik>
        </div>
    );
}