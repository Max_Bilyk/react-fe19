import {request} from "./core"

class VehicleService {
    static async getVehicles() {
        const vehicles = await request(
            {url: "/vehicles"}
        )
        console.log('Vehicles --->', vehicles);

        return vehicles;
    }
}

export default VehicleService;