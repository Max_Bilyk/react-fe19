class UserService {
    static async authorize({email, password}) {
        return new Promise(resolve =>
            // Emulation of AJAX request on auth.
            setTimeout(() =>
                resolve(
                    {
                        token: Math.floor(Math.random() * 1000000),
                        email,
                    }
                ), 1000)
        );
    }
}

export default UserService;