import {Switch, Route} from 'react-router-dom';
import HomePage from "../../components/pages/Home";
import AuthorizePage from "../../components/pages/Authorize";
import VehiclePage from "../../components/pages/Vehicle";

function MainRoute(props) {
    return (
        <main>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/login' component={AuthorizePage}/>
                <Route path='/vehicles' component={VehiclePage}/>
            </Switch>
        </main>
    );
}

export default MainRoute;