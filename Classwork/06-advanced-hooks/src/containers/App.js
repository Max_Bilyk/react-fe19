import MainRoute from "./Route";
import {Menu} from 'antd';
import {Link} from "react-router-dom";
import {useState} from "react";
import './App.css';

function App() {
    const [current, setCurrent] = useState(null);
    const handleClick = e => {
        setCurrent(e.key)
    }

    return (
        <>
            <header>
                <Menu
                    theme={"light"}
                    onClick={handleClick}
                    selectedKeys={[current]}
                    mode="horizontal">
                    <Menu.Item key="home" danger={true}>
                        <Link to="/">Home</Link>
                    </Menu.Item>

                    <Menu.Item key="login">
                        <Link to="/login">Login</Link>
                    </Menu.Item>

                    <Menu.Item key="vehicles" disabled={true}>
                        <Link to="/vehicles">Vehicles</Link>
                    </Menu.Item>
                </Menu>
            </header>

            <MainRoute/>
        </>
    );
}

export default App;
