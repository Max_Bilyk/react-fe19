import React from 'react';
export const themes = {
    light: {
        background: "#000000",
    },
    dark: {
        background: "#222222"
    }
};
export const ThemeContext = React.createContext(themes.light);