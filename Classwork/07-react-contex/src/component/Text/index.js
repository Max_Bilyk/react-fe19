import {ThemeContext} from "../../context/themeContext";
import {useContext} from "react";

export default function Text(){
    const theme = useContext(ThemeContext);
    return(
        <div style={theme}>
            <p>Hello ---> {theme}</p>
        </div>
    )
}