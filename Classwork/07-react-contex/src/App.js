import logo from './logo.svg';
import './App.css';
import {ThemeContext, themes} from "./context/themeContext"
import Text from "./component/Text"

function App() {
  return (
      <ThemeContext.Provider value={themes.dark}>
            <Text/>
      </ThemeContext.Provider>
  );
}

export default App;
