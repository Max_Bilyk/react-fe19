import React from "react";
import FigureSelector from "../FigureSelector";
import {connect} from "react-redux";
import CalcResult from "../CalcResult";
import './Toolbar.css'
import {setCurrentFigure} from "../../actions/appActions";

class Toolbar extends React.Component {

    onSelectorChange = (value) => {
        this.props.setCurrentFigureAction(value);
    }

    render() {

        return (
            <div className="toolbar-wrapper">
                <FigureSelector figures={this.props.figures} onChange={this.onSelectorChange}/>
                <CalcResult result={100}/>
            </div>
        )
    }
}

const mapStateToProps = store => {
    console.log(store)
    return {
        figures: store.app.figures,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentFigureAction: figure => dispatch (setCurrentFigure(figure))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);