import React from "react";
import PropTypes from "prop-types";

class FigureSelector extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange = (e) => {
        this.props.onChange(e.currentTarget.value)
    }

    render() {
        const {figures} = this.props;

        return (
            <select onChange={this.onChange} name="select-figure" id="figureSelection">
                {figures.map(figure =>
                    <option
                        value={figure.type}
                        key={figure.type}>
                        {figure.title}
                    </option>)
                }
            </select>
        )

    }
}

FigureSelector.propTypes = {
    figures: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
}

export default FigureSelector;
