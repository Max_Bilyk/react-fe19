import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    TYPE_TRIANGLE,
    TYPE_RECTANGLE,
    TYPE_SQUARE,
    Rectangle,
    Square,
    Triangle
} from "../Figures";

class Main extends React.Component {
    render() {
        return (
            <div className="main">
                {
                    this.props.figure === TYPE_RECTANGLE
                    ? <Rectangle/>
                    : this.props.figure === TYPE_SQUARE
                    ? <Square/>
                    : <Triangle/>
                }
            </div>
        )
    }
}

const mapStateToProps = store => {
    return{
        figure: store.app.currentFigure
    }
}

export default connect(mapStateToProps)(Main);