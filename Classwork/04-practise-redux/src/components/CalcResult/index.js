import React from "react";
import PropTypes from "prop-types";
import FigureSelector from "../FigureSelector";

class CalcResult extends React.Component{
    render() {
        return(
            <div className="result-wrapper">
                <p>Result: <strong>{this.props.result}</strong></p>
            </div>
        )
    }
}

CalcResult.propTypes = {
    result: PropTypes.number.isRequired,
}

export default CalcResult;