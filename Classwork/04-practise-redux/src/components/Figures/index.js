import Triangle from "./Triangle"
import Square from "./Square"
import Rectangle from "./Rectangle"

const TYPE_TRIANGLE = 'TRIANGLE';
const TYPE_SQUARE = 'SQUARE';
const TYPE_RECTANGLE = 'RECTANGLE'

export {
    Triangle,
    Square,
    Rectangle,
    TYPE_TRIANGLE,
    TYPE_SQUARE,
    TYPE_RECTANGLE,
}