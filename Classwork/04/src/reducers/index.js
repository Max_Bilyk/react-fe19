import {combineReducers} from "redux";

import {pageReducer} from "./page";
import {userReducer} from "./user";

export const rootReducer = combineReducers({
    page: pageReducer,
    user: userReducer,
});

// export const initialState = {
//     user: {
//         name: 'Max',
//         surname: 'Bilyk',
//         age: 14,
//     },
//     balance: 0,
// }
