import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {store} from "./store/storeConfig";
import './index.css';
import App from './container/App';
import Toolbar from "./components/Toolbar";
import reportWebVitals from './reportWebVitals';


ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <Toolbar/>
          <App/>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();