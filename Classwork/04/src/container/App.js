import React from "react";
import {connect} from 'react-redux';
import {Page} from "../components/Page";
import {User} from "../components/User";
import {setYear} from "../actions/PageActions";
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {user, page, setYearAction} = this.props;

        return (
            <div className="user-status">
                <User name={user.name} surname={user.surname}/>
                <Page
                    year={page.year}
                    lesson={page.lesson}
                    setYear={setYearAction}
                />
            </div>
        )
    }
}

const mapStateToProps = store => ({
    user: store.user,
    page: store.page,
})
const mapDispatchToProps = dispatch => ({
    setYearAction: year => (dispatch(setYear(year))),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);