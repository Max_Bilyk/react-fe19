import React from "react";
import PropTypes from "prop-types";

export class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    onBtnClick = e => {
        const year = +e.currentTarget.innerText;
        this.props.setYear(year);
    }

    render() {
        const {year, lesson} = this.props

        return (
            <>
                <div>
                    <button onClick={this.onBtnClick}>1940</button>
                    <button onClick={this.onBtnClick}>1970</button>
                    <button onClick={this.onBtnClick}>1980</button>
                    <button onClick={this.onBtnClick}>2010</button>
                    <button onClick={this.onBtnClick}>2015</button>
                </div>
                <p>Year: <strong>{year}</strong></p>
                <p>Lesson: <strong>{lesson}</strong></p>
            </>
        )
    }
}

Page.propTypes = {
    year: PropTypes.number.isRequired,
    lesson: PropTypes.string.isRequired,
    setYear: PropTypes.func.isRequired,
}

