import React from "react";
import PropTypes from "prop-types";

export class User extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const {name, surname} = this.props

        return(
            <div>
                <p>Hello, <strong>{name} {surname}</strong></p>
            </div>
        )
    }
}

User.propTypes = {
    name: PropTypes.string.isRequired,
}
