import React from "react";
import {connect} from "react-redux";

class Toolbar extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const {year} = this.props.page;

        return(
            <div>
                Toolbar -> <strong>{year}</strong>
            </div>
        )
    }
}

const mapStateToProps = store => ({
    page: store.page,
})

export default connect(mapStateToProps)(Toolbar);