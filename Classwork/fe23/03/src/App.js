// import React from "react";
//
// class Users extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             users: null,
//             isLoading: false,
//             error: null,
//         }
//     }
//
//     componentDidMount() {
//         this.setState({isLoading: true})
//         fetch('https://jsonplaceholder.typicode.com/users')
//             .then(res => {
//                 if (res.ok) {
//                     return res.json()
//                 }
//
//                 throw new Error("Failed to load")
//             })
//             .then(users => {
//                 this.setState({users: users, isLoading: false})
//             })
//             .catch(e => {
//                 this.setState({error: e.message, isLoading: false})
//             })
//     }
//
//
//     render() {
//         const {users, isLoading, error} = this.state
//
//
//         return (
//             <>
//                 <div>
//                     {isLoading && <div>Loading...</div>}
//                     {error && <div>{error}</div>}
//                     {users && !users.length && <div>NoUsers</div>}
//                     {users && users.map(user => {
//                         return <div key={user.id}>
//                             <h3>Name: {user.name}</h3>
//                             <h4>Email: {user.email}</h4>
//                         </div>
//                     })}
//                 </div>
//             </>
//         )
//     }
// }
//
// class Timer extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             seconds: this.props.startFrom,
//         }
//     }
//
//     componentDidMount() {
//         this.interval = setInterval(() => {
//             this.setState({
//                 seconds: this.state.seconds + 1
//             })
//
//             // this.setState(prevState => {
//             //     return {seconds: prevState.seconds + 1}
//             // })
//         }, 1000);
//     }
//
//     componentWillUnmount() {
//         if (this.interval) {
//             clearInterval(this.interval)
//         }
//     }
//
//     // shouldComponentUpdate(nextProps, nextState, nextContext) {
//     //     if (this.props.startFrom !== nextProps.startFrom){
//     //         if (nextProps.startFrom > this.state.seconds){
//     //           return false;
//     //         }
//     //         return true;
//     //     }
//     //     return true
//     // }
//
//     componentDidUpdate(prevProps, prevState, snapshot) {
//
//         if (prevProps.startFrom !== this.props.startFrom) {
//             this.setState({seconds: this.props.startFrom});
//         }
//     }
//
//     render() {
//         console.log(this.props)
//
//         return <div><h3 style={{color: "red"}}>Timer: {this.state.seconds}</h3></div>
//     }
// }
//
// class App extends React.Component {
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             startFrom: 1
//         }
//     }
//
//     render() {
//         return (
//             <>
//                 <div>
//                     <input
//                         type={"number"}
//                         value={this.state.startFrom}
//                         onChange={(e) => {
//                             this.setState({
//                                 startFrom: +e.target.value
//                             })
//                         }}
//                     />
//                     <Timer startFrom={this.state.startFrom}/>
//                     <Users/>
//                 </div>
//             </>
//         )
//     }
//
// }
//
// export default App;