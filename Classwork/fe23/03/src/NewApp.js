import React from "react";
import './App.css'
class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: null,
            isLoading: false,
            error: null,
            loadingNextPage: false,
        };

        this.mainDivRef = null;
        this.usersRef = {}
    }

    componentDidMount() {
        console.log("this.mainDivRef", this.mainDivRef);
        if (this.mainDivRef) {
            this.mainDivRef.onscroll = (e) => {
                console.log(
                    "e.target.scrollHeight - e.target.scrollTop",
                    e.target.scrollHeight - e.target.scrollTop
                );
                console.log("e.target.clientHeight", e.target.clientHeight);
                const bottom =
                    Math.ceil(e.target.scrollHeight - e.target.scrollTop) ===
                    e.target.clientHeight;
                if (bottom) {
                    this.setState({loadingNextPage: true});
                }
            };
        }

        this.setState({isLoading: true});
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                }

                throw new Error("Failed to load");
            })
            .then((users) => {
                this.setState({users: users, isLoading: false});
            })
            .catch((e) => {
                this.setState({error: e.message, isLoading: false});
            });
    }

    render() {
        const {users, isLoading, error, loadingNextPage} = this.state;

        return (
            <>
                <div>
                    <h3>Search users:</h3>
                    <input
                        type="number"
                        // onChange={({target: {value}}) => {
                        //     if (users.length > 0){
                        //         const userHeight = 78;
                        //         this.mainDivRef?.scrollTo({top: (+value - 1) * userHeight, behavior: "smooth"})
                        //     }
                        // }}
                        onChange={}
                    />
                </div>

                <div>
                    {isLoading && <div>Loading</div>}
                    {error && <div>{error}</div>}
                    {users && !users.length && <div>No users</div>}
                    <div
                        ref={(elem) => {
                            this.mainDivRef = elem;
                        }}
                        style={{height: "200px", overflow: "scroll"}}
                    >
                        {users &&
                        users.map((user) => {
                            return (
                                <div ref={(elem) => {this.usersRef[user.id] = elem}} key={user.id}>
                                    <h3>{user.name}</h3>
                                    <div>{user.email}</div>
                                    <div>{user.phone}</div>
                                </div>
                            );
                        })}
                    </div>
                    {loadingNextPage && <div>loading Next Page</div>}
                </div>
            </>
        );
    }
}

class Timer extends React.Component {
    constructor(props) {
        super();

        this.state = {
            seconds: props.startFrom,
        };
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            this.setState((prevState) => {
                return {
                    seconds: prevState.seconds + 1,
                };
            });
        }, 1000);
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.startFrom !== nextProps.startFrom) {
            if (nextProps.startFrom > this.state.seconds) {
                return false;
            }

            return true;
        }

        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.startFrom !== this.props.startFrom) {
            this.setState({seconds: this.props.startFrom});
        }
    }

    render() {
        console.log("startFrom", this.props.startFrom);
        return this.state.seconds;
    }
}

class NewApp extends React.Component {
    constructor() {
        super();

        this.state = {
            startFrom: 1,
        };
    }

    render() {
        return (
            <div>
                <input
                    type={"number"}
                    value={this.state.startFrom}
                    onChange={(event) => {
                        this.setState({startFrom: +event.target.value});
                    }}
                />
                <Timer startFrom={this.state.startFrom}/>
                <Users/>
            </div>
        );
    }
}

export default NewApp;