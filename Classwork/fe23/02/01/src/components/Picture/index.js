import React from "react";

class Picture extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {src, onImageClick} = this.props

        return <img src={src} alt={"picture"} onClick={onImageClick}/>
    }
}

export default Picture;