import React from "react";
import Picture from "../Picture";

const Preview = props => {
    const {url, viewsCount, onBackClick} = props;

    return (
        <div>
            <div>
                <button onClick={onBackClick}>Back</button>
            </div>
            <div>
                <img src={url} alt="picture"/>
            </div>
            <div>
                <p>Viewed: {viewsCount}</p>
            </div>
        </div>
    )
}
export default Preview;