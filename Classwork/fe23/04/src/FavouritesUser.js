import React, { useEffect, useState } from "react";

const Tab = (props) => {
    const { active, text, onSelect } = props;
    return (
        <div
            onClick={onSelect}
            style={{ borderBottom: active ? "2px solid black" : "" }}
        >
            {text}
        </div>
    );
};

const Menu = (props) => {
    const { activeItem, onSelectMenuItem } = props;

    return (
        <div style={{ display: "flex" }}>
            <Tab
                onSelect={() => {
                    onSelectMenuItem("users");
                }}
                text={"Users"}
                active={activeItem === "users"}
            />
            <Tab
                onSelect={() => {
                    onSelectMenuItem("fav");
                }}
                text={"Favourites"}
                active={activeItem === "fav"}
            />
        </div>
    );
};

const Users = (props) => {
    const { users, favUsersIds, onChangedFavUsers } = props;
    return (
        <div>
            {users.map((user) => (
                <div
                    key={user.id}
                    onClick={() => {
                        const found = favUsersIds.includes(user.id);

                        if (found) {
                            onChangedFavUsers(favUsersIds.filter((n) => n !== user.id));
                        } else {
                            onChangedFavUsers([...favUsersIds, user.id]);
                        }
                    }}
                >
                    {user.name} <span>[{favUsersIds.includes(user.id) ? "X" : " "}]</span>
                </div>
            ))}
        </div>
    );
};

const Favs = (props) => {
    const { users } = props;
    return (
        <div>
            <div>Total: {users.length}</div>
            <div>
                {users.map((u) => (
                    <div key={u.id}>{u.name}</div>
                ))}
            </div>
        </div>
    );
};

const FavouriteUsers = () => {
    const [activeSection, setActiveSection] = useState("users"); //users | fav
    const [users, setUsers] = useState(null);
    const [error, setError] = useState(null);
    const ids = localStorage.getItem("ids")
        ? JSON.parse(localStorage.getItem("ids"))
        : [];
    const [favUsersIds, setFavUsersIds] = useState(ids);

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                }

                throw new Error("Failed to load");
            })
            .then((users) => {
                setUsers(users);
            })
            .catch((e) => {
                setError(e.message);
            });
    }, []);

    const renderContainer = () => {
        if (error) {
            return <div>{error}</div>;
        }

        if (!users) {
            return <div>Loading...</div>;
        }

        if (users.length === 0) {
            return <div>No users</div>;
        }

        if (activeSection === "users") {
            return (
                <Users
                    users={users}
                    favUsersIds={favUsersIds}
                    onChangedFavUsers={(ids) => {
                        localStorage.setItem("ids", JSON.stringify(ids));
                        setFavUsersIds(ids);
                    }}
                />
            );
        }

        if (activeSection === "fav") {
            return (
                <Favs
                    users={users.filter((u) => {
                        if (favUsersIds.includes(u.id)) {
                            return true;
                        }

                        return false;
                    })}
                />
            );
        }

        return null;
    };

    return (
        <div>
            <Menu
                activeItem={activeSection}
                onSelectMenuItem={(section) => {
                    setActiveSection(section);
                }}
            />
            <div>{renderContainer()}</div>
        </div>
    );
};

export default FavouriteUsers;
