import {createStore} from "redux";

const INITIAL_STATE = {
    language: 'ru',
    theme: 'dark',
    users: [],
}

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'CHANGE_LANGUAGE':
            return {...state, language: action.payload};
        case 'ADD_USER_TO_LIST':
            return {...state, users: [...state.users, action.payload]};
    }
}

const store = createStore(reducer);


function App() {
    return (
<div>
    <h1>Hello world</h1>
    <button onClick={() => {
        store.dispatch({type: 'CHANGE_LANGUAGE', payload: 'id'})
        store.dispatch({type: 'ADD_USER_TO_LIST', payload: {id: Math.ceil(Math.random() * 100), name: 'Ivan'}})
        console.log(store.getState())
    }}>add user</button>
</div>
    );
}

export default App;
