import React from "react";

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            counter1: 0,
            counter2: 0,
        }
    }

    // handleIncrement1 = () => {
    //     this.setState({counter1: this.state.counter1 + 1})
    // }
    // handleDecrement1 = () => {
    //     if (this.state.counter1) {
    //         this.setState({counter1: this.state.counter1 - 1})
    //     }
    // }
    //
    // handleIncrement2 = () => {
    //     this.setState({counter2: this.state.counter2 + 1})
    // }
    // handleDecrement2 = () => {
    //     if (this.state.counter2) {
    //         this.setState({counter2: this.state.counter2 - 1})
    //     }
    // }

    handleButton = (counterNumber, isIncrement) => {
        return () => {
            if (isIncrement) {
                this.setState({
                    ['counter' + counterNumber]: this.state['counter' + 1]
                })
                // return;
            }else{
                this.setState({
                    ['counter' + counterNumber]: this.state['counter' - 1]
                })
            }
        }
    }

    render() {
        const {counter1, counter2} = this.state

        const sum = counter1 + counter2;

        return (
            <>
                <h1>Counters</h1>

                <div>
                    <button onClick={this.handleButton(1, true)}>+</button>
                    <span>{counter1}</span>
                    <button onClick={this.handleButton(1, false)}>-</button>
                </div>
                <div>
                    <button onClick={this.handleButton(2, true)}>+</button>
                    <span>{counter2}</span>
                    <button onClick={this.handleButton(2, false)}>-</button>
                </div>

                <div>Sum: {sum}</div>
            </>
        )
    }
}

export default App;