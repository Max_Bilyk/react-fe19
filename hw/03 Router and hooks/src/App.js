import './App.css';
import Navigation from "./Router";

function App() {
    return (
        <div className="container">
            <Navigation/>
        </div>
    )
}

export default App;
