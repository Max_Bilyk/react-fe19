import React from "react";
import {BrowserRouter, Route, Link} from "react-router-dom";
import {RoutHeader, RoutLinksList} from '../styles/routLinksStyles';
import Home from "./Home";
import Cart from "./Cart";
import Favourites from "./Favourites";

function Navigation () {
    return(
        <>
            <BrowserRouter>
                <RoutHeader>
                    <nav>
                        <RoutLinksList>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/cart">Cart</Link></li>
                            <li><Link to="/favourites">Favourites</Link></li>
                        </RoutLinksList>
                    </nav>
                </RoutHeader>


                <Route exact={true} path="/" component={Home}/>
                <Route path="/cart" component={Cart}/>
                <Route path="/favourites" component={Favourites}/>
            </BrowserRouter>
        </>
    )
}

export default Navigation;