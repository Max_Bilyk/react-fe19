import {CardImage, Card, CardBody, CardButtonsWrap,} from '../../styles/cardStyles'
import {Icon} from "../../styles/favIconStyle";
import Button from '../Button';
import ModalContainerAdd from "../../containers/modalContainerAdd";
import ModalContainerRemove from "../../containers/modalContainerRemove";
import {useState} from "react";


function ProdCard(props) {
    const {
        src,
        productName,
        color,
        price,
        vendorCode,
        card,
        setToFav,
        onChangeFavProducts,
        favProductsIds,
        removeFromStorage,
        favouritesProducts,
        onChangeCartProducts,
        setToCart,
        cartProducts,
        cartProductsIds
    } = props;

    const [isModalActive, setModalActive] = useState(false);
    const [modalId, setModalId] = useState(null);

    const showModal = e => {
        setModalActive(true);
        setModalId(e.target.dataset.modal);
        console.log(e.target.dataset.modal);
    }

    const hideModal = () => {
        setModalActive(false);
    }

    const foundCartId = cartProductsIds.includes(card.id);

    return (
        <Card className="card" style={{width: '18rem'}}>
            <CardImage className="card-img-top" src={src}/>
            <CardBody className="card-body">
                <h5 className="card-title">{productName}</h5>
                <p className="card-text">Color: {color}</p>
                <p className="card-text">Price: {price}</p>
                <p className="card-text">Vendor Code: {vendorCode}</p>

                <CardButtonsWrap>
                    {!foundCartId &&
                    <>
                        <Button
                            dataModal={`modal${card.id}`}
                            btnClass="btn card-btn"
                            onClick={showModal}
                            bgColor="#0a58ca"
                            btnText="Add to cart"
                            textColor="#ffffff"
                        />
                        <ModalContainerAdd
                            id={`modal${card.id}`}
                            activeState={isModalActive}
                            onModalHide={hideModal}
                            onAddToCartClick={() => {
                                onChangeCartProducts([...cartProductsIds, card.id]);
                                setToCart(card)
                                hideModal();
                            }}
                        />
                    </>}
                    {foundCartId &&
                    <>
                        <Button
                            dataModal={`modal${card.id}`}
                            btnClass="btn card-btn"
                            onClick={showModal}
                            bgColor="#dc3545"
                            btnText="Remove from cart"
                            textColor="#ffffff"
                        />
                        <ModalContainerRemove
                            id={`modal${card.id}`}
                            activeState={isModalActive}
                            onModalHide={hideModal}
                            onRemoveFromCartClick={() => {
                                onChangeCartProducts(cartProductsIds.filter(n => n !== card.id));
                                removeFromStorage(card, cartProducts);
                                hideModal();}
                            }
                        />
                    </>}
                    <button className="btn btn-warning"
                            onClick={() => {
                                const foundId = favProductsIds.includes(card.id);
                                if (foundId) {
                                    onChangeFavProducts(favProductsIds.filter(n => n !== card.id));
                                    removeFromStorage(card, favouritesProducts);
                                }else {
                                    onChangeFavProducts([...favProductsIds, card.id]);
                                    setToFav(card)
                                }
                            }}
                    >
                        <Icon className={favProductsIds.includes(card.id) ? 'fas fa-star' : 'far fa-star'}/>
                    </button>
                </CardButtonsWrap>
            </CardBody>
        </Card>
    )
}

export default ProdCard;