import {CardWrap} from '../../styles/cardListStyles';
import React, {useState, useEffect} from "react";
import ProdCard from "../ProdCard";
import axios from "axios";


function ProdList(props) {

    const {routeRender} = props;

    const favIds = localStorage.getItem('favIds')
        ? JSON.parse(localStorage.getItem('favIds'))
        : []

    const favItem = localStorage.getItem('Favourites')
        ? JSON.parse(localStorage.getItem('Favourites'))
        : []

    const cartIds = localStorage.getItem('cartIds')
        ? JSON.parse(localStorage.getItem('cartIds'))
        : []

    const cartItem = localStorage.getItem('Cart')
        ? JSON.parse(localStorage.getItem('Cart'))
        : []

    const [product, setProduct] = useState([]);
    const [favouritesProducts, setFavouritesProducts] = useState(favItem);
    const [cartProducts, setCartProducts] = useState(cartItem);
    const [favProductsIds, setFavProductsIds] = useState(favIds);
    const [cartProductsIds, setCartProductsIds] = useState(cartIds);

    useEffect(() => {
        axios.get('http://localhost:3000/products.json')
            .then(({data}) => {
                setProduct(data);
            })
    }, []);

    useEffect(() => {
        localStorage.setItem("Favourites", JSON.stringify(favouritesProducts));
    }, [favouritesProducts]);
    useEffect(() => {
        localStorage.setItem('Cart', JSON.stringify(cartProducts));
    }, [cartProducts])

    const setToFav = item => {
        setFavouritesProducts([...favouritesProducts, item]);
    };
    const onChangeFavProducts = ids => {
        localStorage.setItem('favIds', JSON.stringify(ids));
        setFavProductsIds(ids);
    };

    const setToCart = item => {
        setCartProducts([...cartProducts, item]);
    }
    const onChangeCartProducts = ids => {
        localStorage.setItem('cartIds', JSON.stringify(ids));
        setCartProductsIds(ids);
    }

    const removeFromStorage = (item, removeItem) => {
        const itemId = item.id;

        if (removeItem === favouritesProducts){
            setFavouritesProducts(favouritesProducts.filter(({id}) => itemId !== id));
        }else if (removeItem === cartProducts){
            setCartProducts(cartProducts.filter(({id}) => itemId !== id));
        }
    }


    return (
        <CardWrap className="card-wrapper">
            {routeRender === 'home' && product.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromStorage={removeFromStorage}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProducts}
                    cartProducts={cartProducts}
                    cartProductsIds={cartProductsIds}
                />)}
            {routeRender === 'favourites' && favouritesProducts.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromStorage={removeFromStorage}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProducts}
                    cartProducts={cartProducts}
                    cartProductsIds={cartProductsIds}
                />
            )}
            {routeRender === 'cart' && cartProducts.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromStorage={removeFromStorage}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProducts}
                    cartProducts={cartProducts}
                    cartProductsIds={cartProductsIds}
                />
            )}

        </CardWrap>)
}

export default ProdList;