function Button(props) {
    const {dataModal, bgColor, btnText, onClick, btnClass, textColor, id,} = props;

    return (
        <>
            <button
                data-modal={dataModal}
                style={{background: bgColor, color: textColor}}
                className={btnClass}
                onClick={onClick}
                id={id}
            >   {btnText}
            </button>
        </>
    )
}

export default Button;