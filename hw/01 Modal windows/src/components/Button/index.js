import React from "react";

class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {dataModal, bgColor, btnText, onClick, btnClass, textColor,} = this.props;

        return (
            <>
                <button
                    data-modal={dataModal}
                    style={{background: bgColor, color: textColor}}
                    className={btnClass}
                    onClick={onClick}
                >   {btnText}
                </button>
            </>
        )
    }
}

export default Button;