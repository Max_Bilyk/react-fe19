
import styled from 'styled-components'

export const ModalHeader = styled.div`
  background-color: ${props => props.modalTheme.headerBg};
  color: ${props => props.modalTheme.headerColor};
`

export const ModalBody = styled.div`
  background-color: ${props => props.modalTheme.mainBg};
  color: ${props => props.modalTheme.mainColor};
`

export const ActionsButtonWrap = styled.div`
  display: flex;
  justify-content: center;
  background-color: ${props => props.modalTheme.footerBg};
`

export const ModalWindow = styled.div`
  height: 100%;
  width: 100%;
  background: rgba(0, 0, 0, 0.5);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  pointer-events: none;
  transition: 0.5s;
`
export const ModalContent = styled.div`
  border-radius: 5px;
  width: 517px;
  margin: -150px 0 0 0;
  transform: scale(0.3);
  transition: 0.4s all;
`
export const CloseBtn = styled.button`
  position: relative;
`
export const Close = styled.span`
  position: absolute;
  top: -29px;
  right: -4px;
  font-size: 40px;
  color: ${props => props.modalTheme.closeBtnColor};
`