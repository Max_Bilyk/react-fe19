import './App.css';
import Navigation from "./Router";
import {useEffect} from "react";
import {connect, useDispatch} from "react-redux";
import {loadDataAction} from "./redux/actions/loadDataAction";
import {addToFavouritesAction} from "./redux/actions/favouritesAction";
import {addToCartAction} from "./redux/actions/cartAction";

function App() {

  const favouriteData = JSON.parse(localStorage.getItem('favourites'));
  const cartData = JSON.parse(localStorage.getItem('cart'));
  const dispatch = useDispatch();

  useEffect(() => {
    if (favouriteData !== null) {
      dispatch(addToFavouritesAction(favouriteData));
    }
    if (cartData !== null){
      dispatch(addToCartAction(cartData));
    }
  }, []);

  useEffect(() => {
    dispatch(loadDataAction())
  }, [dispatch]);


  return (
      <div className="container">
        <Navigation/>
      </div>
  )
}

export default connect(state => ({
  favourites: state.favouritesReducer.favourites,
  cart: state.cartReducer.cart,
  products: state.productsReducer.products,
}))(App);
