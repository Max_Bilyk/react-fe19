import React from 'react';
import {useFormik} from 'formik';
import * as yup from 'yup';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import '../Form/form.css';
import {connect, useSelector} from "react-redux";
import {checkoutCartAction} from "../../redux/actions/cartAction";
import {checkoutUserDataAction} from '../../redux/actions/checkoutUserDataAction';
import {useDispatch} from "react-redux";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const checkoutFormValidationSchema = yup.object({
    name: yup
        .string('Enter your name')
        .min(3, 'Name should be of minimum 3 characters length')
        .max(17, 'Name it too long')
        .required('Name is required'),
    secondName: yup
        .string('Enter your second name')
        .min(3, 'Second name should be of minimum 3 characters length')
        .max(17, 'Second name it too long')
        .required('Second name is required'),
    age: yup
        .number('Enter your age')
        .required('Age is required')
        .positive('Age must be a positive number')
        .max(80, 'You are too old')
        .integer('Age must be a number'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    phoneNumber: yup
        .string('Enter your telephone number')
        .matches(phoneRegExp, 'Phone number is not valid')
        .required('Email is required'),
    address: yup
        .string('Enter your home address')
        .min(5, 'Address is too short')
        .max(50, 'Address it too long')
        .required('Email is required'),
});

function CheckoutForm({cart}) {
    const checkoutData = useSelector(state => state.checkoutReducer.checkoutData)
    const dispatch = useDispatch();

    const formik = useFormik({
        initialValues: {
            name: '',
            secondName: '',
            age: '',
            email: '',
            phoneNumber: '',
            address: '',
        },
        validationSchema: checkoutFormValidationSchema,
        onSubmit: values => {
            checkoutData.push(values);
            console.log('bought products --->', cart);
            dispatch(checkoutCartAction());
            dispatch(checkoutUserDataAction(checkoutData));
            console.log('data from form --->', values);
        }
    });

    return (
        <div className="checkout-form">

            <h3 style={{textAlign: "center"}}>Checkout</h3>

            <form onSubmit={formik.handleSubmit}>
                <TextField
                    fullWidth
                    id="name"
                    name="name"
                    label="Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <TextField
                    fullWidth
                    id="secondName"
                    name="secondName"
                    label="Second name"
                    value={formik.values.secondName}
                    onChange={formik.handleChange}
                    error={formik.touched.secondName && Boolean(formik.errors.secondName)}
                    helperText={formik.touched.secondName && formik.errors.secondName}
                />
                <TextField
                    fullWidth
                    id="age"
                    name="age"
                    label="Age"
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    helperText={formik.touched.age && formik.errors.age}
                />
                <TextField
                    fullWidth
                    id="email"
                    name="email"
                    label="Email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                />
                <TextField
                    fullWidth
                    id="phoneNumber"
                    name="phoneNumber"
                    label="Phone Number"
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                    error={formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)}
                    helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
                />
                <TextField
                    fullWidth
                    id="address"
                    name="address"
                    label="Address"
                    value={formik.values.address}
                    onChange={formik.handleChange}
                    error={formik.touched.address && Boolean(formik.errors.address)}
                    helperText={formik.touched.address && formik.errors.address}
                />

                <div className="form-btn-container">
                    <Button color="primary" variant="contained" fullWidth type="submit">Checkout</Button>
                </div>
            </form>
        </div>
    )
}

export default connect(state => ({
    cart: state.cartReducer.cart,
}))(CheckoutForm);