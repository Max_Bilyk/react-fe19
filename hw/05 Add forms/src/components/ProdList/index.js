import {CardWrap} from '../../styles/cardListsStyles';
import {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {
    addToFavouritesAction,
    removeFromFavouritesAction
} from "../../redux/actions/favouritesAction";
import {
    addToCartAction,
    removeFromCartAction,
} from "../../redux/actions/cartAction";
import ProdCard from "../ProdCard";

function ProdList(props) {
    const {routeRender} = props;

    const favIds = localStorage.getItem('favIds')
        ? JSON.parse(localStorage.getItem('favIds'))
        : [];
    const cartIds = localStorage.getItem('cartIds')
        ? JSON.parse(localStorage.getItem('cartIds'))
        : [];

    const dispatch = useDispatch();

    const [favProductsIds, setFavProductsIds] = useState(favIds);
    const [cartProductsIds, setCartProductsIds] = useState(cartIds);
    const productList = useSelector(state => state.productsReducer.products);
    const favouritesProductsRedux = useSelector(state => state.favouritesReducer.favourites);
    const cartProductsRedux = useSelector(state => state.cartReducer.cart)

    const setToFav = item => {
        favouritesProductsRedux.push(item);
        dispatch(addToFavouritesAction(favouritesProductsRedux));
        localStorage.setItem('favourites', JSON.stringify(favouritesProductsRedux));
    };

    const setToCart = item => {
        cartProductsRedux.push(item);
        dispatch(addToCartAction(cartProductsRedux));
        localStorage.setItem('cart', JSON.stringify(cartProductsRedux));
    };

    const removeFromFav = item => {
        const itemId = item.id;
        const filteredFavs = favouritesProductsRedux.filter(({id}) => itemId !== id);
        dispatch(removeFromFavouritesAction(filteredFavs));
        localStorage.setItem('favourites', JSON.stringify(filteredFavs));
    };

    const removeFromCart = item => {
        const itemId = item.id;
        const filteredCart = cartProductsRedux.filter(({id}) => itemId !== id);
        dispatch(removeFromCartAction(filteredCart));
        localStorage.setItem('cart', JSON.stringify(filteredCart));
    };

    const onChangeFavProducts = ids => {
        localStorage.setItem('favIds', JSON.stringify(ids));
        setFavProductsIds(ids);
    };

    const onChangeCartProducts = ids => {
        localStorage.setItem('cartIds', JSON.stringify(ids));
        setCartProductsIds(ids);
    };

    return (
        <CardWrap className="card-wrapper">
            {routeRender === 'home' && productList.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromFav={removeFromFav}
                    removeFromCart={removeFromCart}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProductsRedux}
                    cartProducts={cartProductsRedux}
                    cartProductsIds={cartProductsIds}
                />)}
            {routeRender === 'favourites' && favouritesProductsRedux !== null && favouritesProductsRedux.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromFav={removeFromFav}
                    removeFromCart={removeFromCart}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProductsRedux}
                    cartProducts={cartProductsRedux}
                    cartProductsIds={cartProductsIds}
                />
            )}
            {routeRender === 'cart' && cartProductsRedux.map(item =>
                <ProdCard
                    src={item.src}
                    productName={item.productName}
                    color={item.color}
                    price={item.price}
                    vendorCode={item.vendorCode}
                    key={item.id}
                    card={item}
                    setToFav={setToFav}
                    setToCart={setToCart}
                    onChangeFavProducts={onChangeFavProducts}
                    onChangeCartProducts={onChangeCartProducts}
                    removeFromFav={removeFromFav}
                    removeFromCart={removeFromCart}
                    favProductsIds={favProductsIds}
                    favouritesProducts={favouritesProductsRedux}
                    cartProducts={cartProductsRedux}
                    cartProductsIds={cartProductsIds}
                />
            )}

        </CardWrap>)
}

export default ProdList;