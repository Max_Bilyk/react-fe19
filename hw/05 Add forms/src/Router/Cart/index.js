import ProdList from "../../components/ProdList";
import CheckoutForm from "../../components/Form";

const Cart = () => {

    const cart = JSON.parse(localStorage.getItem('cart'));
    return (
        <>
            {cart === null || cart.length < 1 ? <h3>You have no items in cart</h3> : <h3>Cart - {cart.length} items</h3>}
            <ProdList routeRender={'cart'}/>

            {
                cart && cart.length > 0 && <CheckoutForm/>
            }

        </>
    )
}

export default Cart;