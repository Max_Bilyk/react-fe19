import ProdList from '../../components/ProdList'

const Favourites = () => {

    const favourites = JSON.parse(localStorage.getItem('favourites'));

    return (
        <>
            {favourites === null || favourites.length < 1 ? <h3>You have no favourites items</h3> : <h3>You have {favourites.length} favourites items</h3>}
            <ProdList routeRender={'favourites'}/>
        </>
    )
}

export default Favourites;