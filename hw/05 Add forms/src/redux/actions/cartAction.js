export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CHECKOUT_CART = 'CHECKOUT_CART';

export const addToCartAction = item => dispatch => {
    dispatch({
        type: ADD_TO_CART,
        payload: item,
    });
};

export const removeFromCartAction = cartArr => dispatch => {
    dispatch({
        type: REMOVE_FROM_CART,
        payload: cartArr,
    });
};

export const checkoutCartAction = () => dispatch => {
    localStorage.setItem('cart', '[]');
    localStorage.setItem('cartIds', '[]')

    dispatch({
        type: CHECKOUT_CART,
        payload: []
    });
};