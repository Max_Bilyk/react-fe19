import {loadProductsData} from "../../services/loadDataService";

export const LOAD_PRODUCTS_DATA = 'LOAD_PRODUCTS_DATA';

export const loadDataAction = () => async dispatch => {
    const {data} = await loadProductsData();

    dispatch({
        type: LOAD_PRODUCTS_DATA,
        payload: data,
    });
};