export const CHECKOUT_USER_DATA = 'CHECKOUT_USER_DATA';

export const checkoutUserDataAction = formData => dispatch => {
    dispatch({
        type: CHECKOUT_USER_DATA,
        payload: formData,
    });
};