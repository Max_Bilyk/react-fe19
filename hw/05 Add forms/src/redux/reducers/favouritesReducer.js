import {ADD_TO_FAVOURITES, REMOVE_FROM_FAVOURITES} from "../actions/favouritesAction";

const initialState = {
    favourites: [],
}

export default function favouritesReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_FAVOURITES :
            // console.log('add to fav --->', state)
            return {
                ...state,
                favourites: action.payload,
            }
        case REMOVE_FROM_FAVOURITES :
            // console.log('remove from favs -->', state)
            return {
                favourites: action.payload
            }
        default:
            return state;
    }
}