import productDataReducer from "./productsDataReducer";
import modalReducer from "./modalReducer";
import favouritesReducer from "./favouritesReducer";
import cartReducer from "./cartReducer";
import checkoutUserDataReducer from "./checkoutUserDataReducer";
import {combineReducers} from "redux";

export const rootReducer = combineReducers
({
    productsReducer: productDataReducer,
    modalWindowReducer: modalReducer,
    favouritesReducer: favouritesReducer,
    cartReducer: cartReducer,
    checkoutReducer: checkoutUserDataReducer
})
