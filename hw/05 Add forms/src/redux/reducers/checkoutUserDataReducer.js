import {CHECKOUT_USER_DATA} from "../actions/checkoutUserDataAction";

const initialState = {
    checkoutData: [],
}

export default function checkoutUserDataReducer(state = initialState, action) {
    switch (action.type) {
        case CHECKOUT_USER_DATA:
            return {
                ...state,
                checkoutData: action.payload
            }
        default:
            return state;
    }
}