import axios from "axios";

export const loadProductsData = () => {
    return axios.get('http://localhost:3000/products.json')
}