import React from "react";

class Button extends React.Component {
    render() {
        const {
            dataModal,
            bgColor,
            btnText,
            onClick,
            btnClass,
            textColor,
            id,
        } = this.props;

        return (
            <>
                <button
                    data-modal={dataModal}
                    style={{background: bgColor, color: textColor}}
                    className={btnClass}
                    onClick={onClick}
                    id={id}
                >   {btnText}
                </button>
            </>
        )
    }
}

export default Button;