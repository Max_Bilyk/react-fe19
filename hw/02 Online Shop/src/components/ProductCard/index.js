import React from "react";
import {CardImage, Card, CardBody, CardButtonsWrap,} from '../../styles/cardStyles'
import Button from "../Button";
import ModalContainer from "../../containers/ModalContainer";
import {Icon} from "../../styles/favIcons";
import PropTypes from "prop-types";

class ProdCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isInCart: false,
            modalActive: false,
            setActiveModal: false,
            modalId: null,
        }

        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    async addToCart() {
        await this.setState({isInCart: !this.state.isInCart});
        const {cartArr} = this.props;
        console.log('Added to cart --->', cartArr)
        localStorage.setItem("Cart", JSON.stringify(cartArr));
    }

    showModal(e) {
        this.setState({
            modalActive: true,
            setActiveModal: true,
            modalId: e.target.dataset.modal,
        });
    }

    hideModal() {
        this.setState({
            modalActive: false,
            setActiveModal: false,
        })
    }

    render() {
        const {
            productName,
            price,
            src,
            vendorCode,
            color,
            card,
            setToFav,
            removeFromFav,
            setToCart,
            isFav,
            favProductsIds,
        } = this.props;

        return (
            <Card className="card" style={{width: '18rem'}}>
                <CardImage className="card-img-top" src={src}/>
                <CardBody className="card-body">
                    <h5 className="card-title">{productName}</h5>
                    <p className="card-text">Color: {color}</p>
                    <p className="card-text">Price: {price}</p>
                    <p className="card-text">Vendor Code: {vendorCode}</p>
                    <CardButtonsWrap>
                        <Button
                            dataModal={`modal${card.id}`}
                            btnClass="btn card-btn"
                            onClick={this.showModal}
                            bgColor="#0a58ca"
                            btnText="Add to cart"
                            textColor="#ffffff"
                        />
                        <ModalContainer
                            activeState={this.state.modalActive}
                            onModalHide={this.hideModal}
                            id={`modal${card.id}`}
                            onAddToCartClick={() => {
                                !this.state.isInCart &&
                                setToCart(card);
                                this.addToCart();
                                this.hideModal();
                            }}/>


                        <button className="btn btn-warning"
                                onClick={() => {
                                    const found = favProductsIds.includes(card.id);

                                    if (found) {
                                        removeFromFav(card);
                                        isFav(favProductsIds.filter((n) => n !== card.id));
                                    } else {
                                        isFav([...favProductsIds, card.id]);
                                        setToFav(card);
                                    }
                                }}
                        >
                            <Icon
                                className={favProductsIds.includes(card.id) ? 'fas fa-star' : 'far fa-star'}
                            />
                        </button>

                    </CardButtonsWrap>
                </CardBody>
            </Card>
        )
    }
}

ProdCard.propTypes = {
    productName: PropTypes.string,
    price: PropTypes.string,
    src: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    card: PropTypes.object,
    setToFav: PropTypes.func,
    removeFromFav: PropTypes.func,
    setToCart: PropTypes.func,
    favArr: PropTypes.array,
    cartArr: PropTypes.array,
}

export default ProdCard;
