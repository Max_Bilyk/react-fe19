import React from "react";
import axios from "axios";
import ProdCard from "../ProductCard";
import {CardWrap} from '../../styles/listStyles';

class ProdList extends React.Component {
    constructor(props) {
        super(props);

        const ids = localStorage.getItem("FavProductsIds")
            ? JSON.parse(localStorage.getItem("FavProductsIds"))
            : [];

        this.state = {
            cardData: [],
            favourites: [],
            inCart: [],
            favProductsIds: ids,
        }

        this.setToFav = this.setToFav.bind(this);
        this.setToCart = this.setToCart.bind(this);
        this.removeFromFav = this.removeFromFav.bind(this);
        this.isFavouriteProducts = this.isFavouriteProducts.bind(this);
    }

    async componentDidMount() {
        const {data} = await axios.get('');
        console.log(data);
        this.setState({cardData: data});
    }

    async setToFav(item) {
        // item - product card
        await this.setState(prevState => ({favourites: [...prevState.favourites, item]}));
        localStorage.setItem("Favourite", JSON.stringify(this.state.favourites));
        console.log('Added to favs --->', JSON.parse(localStorage.getItem('Favourite')));
    }

    async isFavouriteProducts (ids)  {
        localStorage.setItem('FavProductsIds', JSON.stringify(ids));
        await this.setState({favProductsIds: ids})
    }

    async setToCart(item) {
        await this.setState(prevState => ({inCart: [...prevState.inCart, item]}))
    }

    removeFromFav(item) {
        let idx = item.id;
        let favList = this.state.favourites;
        this.setState(() => {
            const favourites = favList.filter(item => item.id !== idx);
            console.log('Remove --->', favourites);
            return {favourites};
        });
        localStorage.removeItem("Favourite")
    }


    render() {
        return (
            <CardWrap className="card-wrapper">
                {this.state.cardData.map(item =>
                    <ProdCard
                        src={item.src}
                        productName={item.productName}
                        color={item.color}
                        price={item.price}
                        vendorCode={item.vendorCode}
                        key={item.id}
                        card={item}
                        id={item.id}
                        setToFav={this.setToFav}
                        removeFromFav={this.removeFromFav}
                        // favArr={this.state.favourites}
                        cartArr={this.state.inCart}
                        setToCart={this.setToCart}
                        isFav={this.isFavouriteProducts}
                        favProductsIds={this.state.favProductsIds}
                    />)
                }
            </CardWrap>
        )
    }
}

export default ProdList;
