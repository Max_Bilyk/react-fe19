import styled from "styled-components";

export const CardImage = styled.img`
  max-width: 200px;
  height: 150px;
`

export const Card = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 400px;
`

export const CardBody = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

export const CardButtonsWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`