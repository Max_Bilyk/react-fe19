import styled from 'styled-components'


export const RoutLinksList = styled.ul`
  display: flex;
  justify-content: space-around;
  list-style: none;
`

export const RoutHeader = styled.header`
  width: 100%;
  background-color: lightblue;
  padding: 20px 20px 10px 20px;
  margin: 0 0 10px 0;
`