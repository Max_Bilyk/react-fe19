import ProdList from '../../components/ProdList'

const Favourites = () => {
    return (
        <>
            <h3>Favourites</h3>
            <ProdList routeRender={'favourites'}/>
        </>
    )
}

export default Favourites;