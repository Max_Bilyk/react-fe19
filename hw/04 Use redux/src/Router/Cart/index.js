import ProdList from "../../components/ProdList";

const Cart = () => {
    return (
        <>
            <h3>Cart</h3>
            <ProdList routeRender={'cart'}/>
        </>
    )
}

export default Cart;