import ProdList from "../../components/ProdList";

const Home = () => {

    return (
        <>
            <h3>Home</h3>
            <ProdList routeRender={'home'}/>
        </>
    )
}

export default Home;