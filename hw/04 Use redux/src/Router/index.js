import React from "react";
import {BrowserRouter, Route, Link} from "react-router-dom";
import {RoutHeader, RoutLinksList} from '../styles/routLinks';
import Home from "./Home";
import Cart from "./Cart";
import Favourites from "./Favourites";
import {Switch} from "react-router";

function Navigation () {
    return(
        <>
            <BrowserRouter>
                <RoutHeader>
                    <nav>
                        <RoutLinksList>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/cart">Cart</Link></li>
                            <li><Link to="/favourites">Favourites</Link></li>
                        </RoutLinksList>
                    </nav>
                </RoutHeader>

            <Switch>
                <Route exact={true} path="/" component={Home}/>
                <Route path="/cart" component={Cart}/>
                <Route path="/favourites" component={Favourites}/>
            </Switch>
            </BrowserRouter>
        </>
    )
}

export default Navigation;