import './App.css';
import Navigation from "./Router";
import {useEffect} from "react";
import {connect, useDispatch} from "react-redux";
import {addToCartAction} from "./redux/actions/cartAction";
import {addToFavouritesAction} from "./redux/actions/favouritesAction";
import {loadDataAction} from "./redux/actions/loadDataAction";

function App() {
    const dispatch = useDispatch();
    const favouriteData = JSON.parse(localStorage.getItem('favourites'));
    const cartData = JSON.parse(localStorage.getItem('cart'))


    useEffect(() => {
        if (favouriteData !== null) {
            dispatch(addToFavouritesAction(favouriteData));
        }
        if (cartData !== null){
            dispatch(addToCartAction(cartData));
        }
    }, []);

    useEffect(() => {
        dispatch(loadDataAction())
    }, [dispatch]);

    return (
        <div className="container">
            <Navigation/>
        </div>
    )
}

export default connect(state => ({
    favourites: state.favouritesReducer.favourites,
    cart: state.cartReducer.cart,
}))(App);