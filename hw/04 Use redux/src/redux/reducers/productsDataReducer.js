import {LOAD_PRODUCTS_DATA} from "../actions/loadDataAction";

const initialState = {
    products: [],
};

export default function productDataReducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_PRODUCTS_DATA :
            return {
                ...state,
                products: action.payload,
            }
        default:
            return state;
    }
}