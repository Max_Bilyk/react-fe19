import {ADD_TO_CART, REMOVE_FROM_CART} from "../actions/cartAction";

const initialState = {
    cart: [],
}

export default function cartReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_CART:
            return {
                ...state,
                cart: action.payload
            }
        case REMOVE_FROM_CART:
            return {
                cart: action.payload
            }
        default:
            return state;
    }
}