export const ADD_TO_FAVOURITES = 'ADD_TO_FAVOURITES';
export const REMOVE_FROM_FAVOURITES = 'REMOVE_FROM_FAVOURITES';

export const addToFavouritesAction = item => dispatch => {
  dispatch({
      type: ADD_TO_FAVOURITES,
      payload: item,
  });
};

export const removeFromFavouritesAction = favArr => dispatch => {
    dispatch({
        type: REMOVE_FROM_FAVOURITES,
        payload: favArr,
    });
};